package com.sample.chat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ChatServer {
	public static void main(String[] args) {
		System.out.println("チャットサーバーを開始します");
		InetSocketAddress address = new InetSocketAddress(5555);
		
		try (ServerSocket serverSocket = new ServerSocket()){
			System.out.println("接続待ち....");
			serverSocket.bind(address);
			Socket socket = serverSocket.accept();
			System.out.println(String.format("接続されました [%s:%d]",socket.getRemoteSocketAddress(),socket.getLocalPort()));
			
			try(Scanner scanner = new Scanner(socket.getInputStream())){
				while(socket.isConnected()){
					String comment = scanner.nextLine();
					System.out.println("Server Said : " + comment);
				}
			}
		} catch (IOException e) {
			System.out.println("入出力エラーが発生");
			e.printStackTrace();
		} finally {
			System.out.println("チャットサーバーを終了します");
		}
	}
}
