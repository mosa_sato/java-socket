package com.sample.chat;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ChatClient {
	public static void main(String[] args) {
		System.out.println("チャットクライアント開始");
		System.out.println("接続します");
		
		InetSocketAddress address = new InetSocketAddress("localhost", 5555);
		
		try (Socket socket = new Socket()){
			socket.connect(address);
			System.out.println(String.format("接続しました [%s:%d]", socket.getRemoteSocketAddress(),socket.getPort()));
			
			PrintWriter pw = new PrintWriter(socket.getOutputStream());
			try(Scanner scanner = new Scanner(System.in)){
				String comment = null;
				while(!"exit".equals((comment = scanner.nextLine()))){
					pw.println(comment);
					pw.flush();
				}
			}
		} catch (UnknownHostException e) {
			System.out.println("ホストがないってさ");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("入出力エラーだってさ");
			e.printStackTrace();
		} finally {
			System.out.println("チャットクライアント終了");
		}
	}
}
