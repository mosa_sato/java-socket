package com.sample.chat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ChatServer2 {
	public static void main(String[] args) {
		System.out.println("チャットサーバーを開始します");
		InetSocketAddress address = new InetSocketAddress(5555);
		
		ExecutorService es = Executors.newFixedThreadPool(2);
		
		try (ServerSocket serverSocket = new ServerSocket()){
			System.out.println("接続待ち....");
			serverSocket.bind(address);
			Socket socket = serverSocket.accept();
			System.out.println(String.format("接続されました [%s:%d]",socket.getRemoteSocketAddress(),socket.getLocalPort()));
			
			Future<?> f =  es.submit(new Runnable() {
				
				@Override
				public void run() {
					try(Scanner scanner = new Scanner(socket.getInputStream())){
						while(socket.isConnected()){
							String comment = scanner.nextLine();
							System.out.println("Server Said : " + comment);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			
			while(!f.isDone()){
				try {Thread.sleep(5000L);} catch (InterruptedException e) {}
				//System.out.println("私生きてます");
			}
		} catch (IOException e) {
			System.out.println("入出力エラーが発生");
			e.printStackTrace();
		} finally {
			es.shutdown();
			System.out.println("チャットサーバーを終了します");
		}
	}
}
